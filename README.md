# Lillydoo Task

Thank you for the challenge. it was nice one.

## Steps to run the app
- Please execute `docker-compose up --build`
- then please exec `docker exec -it lillydoo-task_php_1 composer install` to install composer.
- then please exec `docker exec -it lillydoo-task_php_1 bin/console doctrine:schema:create` to create schema.
- then please exec `docker exec -it lillydoo-task_php_1 bin/phpunit --coverage-text` to run the tests.
- then please go to `http://localhost:8080/entry/` to access the app itself

## hint

Sorry I didn't have time to integrate a good UI template in the project so it is basic twigs without style.
