<?php
declare(strict_types=1);

namespace App\Tests\Integration\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

final class EntryControllerTest extends WebTestCase
{
    public function testGetAllEntries(): void
    {
        $client = static::createClient();

        $client->request('GET', '/entry/');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }


    public function testShowEntry(): void
    {
        $client = static::createClient();

        $client->request(
            'GET',
            '/entry/1'
        );

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }

    public function testPostEntry(): void
    {
        $client = static::createClient();

        $client->request(
            'POST',
            '/entry/new',
            [
                'firstName' => 'testName',
                'lastName' => 'lastName',
                'email' => 'test@test.com',
                'streetName' => 'street name1',
                'phoneNumber' => '123456789',
                'buildingNumber' => 12,
                'zipCode' => '12345',
                'city' => 'Berlin',
                'country' => 'Germany',
                'birthday' => [
                    'day' => '11',
                    'month' => '10',
                    'year' => '1992',
                ],
            ]
        );

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }

    public function testUpdateEntry(): void
    {
        $client = static::createClient();

        $client->request(
            'POST',
            'entry/1/edit',
            [
                'firstName' => 'testName',
                'lastName' => 'lastName',
                'email' => 'test@test.com',
                'streetName' => 'street name1',
                'phoneNumber' => '123456789',
                'buildingNumber' => 12,
                'zipCode' => '12345',
                'city' => 'Berlin',
                'country' => 'Germany',
                'birthday' => [
                    'day' => '11',
                    'month' => '10',
                    'year' => '1992',
                ],
            ]
        );

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }

    public function testDeleteEntry(): void
    {
        $client = static::createClient();

        $csrfToken = $client->getContainer()->get('security.csrf.token_manager')->getToken('delete1');

        $client->request(
            'DELETE',
            '/entry/1',
            ['_token' => $csrfToken]
        );

        $this->assertEquals(302, $client->getResponse()->getStatusCode());
    }
}
