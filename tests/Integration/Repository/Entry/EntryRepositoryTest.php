<?php
declare(strict_types=1);

namespace App\Tests\Integration\Repository\Entry;

use App\DataFixtures\EntryFixtures;
use App\Entity\Entry;
use App\Repository\Entry\EntryRepository;
use App\Tests\Unit\DataFixture\AddressBookEntriesFixtures;
use Doctrine\ORM\EntityManager;
use Liip\TestFixturesBundle\Test\FixturesTrait;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

final class EntryRepositoryTest extends KernelTestCase
{
    use FixturesTrait;

    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @var EntryRepository
     */
    private $entryRepository;

    /**
     * @var []
     */
    private $entriesFixture;

    /**
     * {@inheritDoc}
     */
    protected function setUp(): void
    {
        $kernel = self::bootKernel();

        $this->entityManager = $kernel->getContainer()
            ->get('doctrine')
            ->getManager();

        $this->entryRepository = new EntryRepository($this->entityManager);

        $this->loadFixtures([EntryFixtures::class]);

        $this->entriesFixture = AddressBookEntriesFixtures::getAddressBookEntriesForTesting();
    }

    public function testFindAll(): void
    {
        $entries = $this->entryRepository->findAll();

        foreach ($entries as $key => $entry) {

            $this->assertEntry($this->entriesFixture[$key], $entry);
        }
    }

    public function testFind(): void
    {
        $entry = $this->entryRepository->find(1);

        $this->assertEntry($this->entriesFixture[0], $entry);
    }

    public function testSaveEntry(): void
    {

        $this->entryRepository->save($this->entriesFixture[2]);

        $this->entityManager->flush();

        $entry = $this->entryRepository->find(3);

        $this->assertEntry($this->entriesFixture[2], $entry);
    }

    public function testDeleteEntry(): void
    {
        $entryToDelete = $this->entryRepository->find(2);

        $this->entryRepository->delete($entryToDelete);

        $this->entityManager->flush();

        $this->assertNull($this->entryRepository->find(2));
    }

    protected function tearDown(): void
    {
        parent::tearDown();

        $this->entityManager->close();
        $this->entityManager = null;
    }

    private function assertEntry(Entry $expectedEntry, Entry $actualEntry)
    {
        $this->assertSame($expectedEntry->getId(), $actualEntry->getId());
        $this->assertSame($expectedEntry->getFirstName(), $actualEntry->getFirstName());
        $this->assertSame($expectedEntry->getLastName(), $actualEntry->getLastName());
        $this->assertSame($expectedEntry->getEmail(), $actualEntry->getEmail());
        $this->assertSame($expectedEntry->getBuildingNumber(), $actualEntry->getBuildingNumber());
        $this->assertSame($expectedEntry->getStreetName(), $actualEntry->getStreetName());
        $this->assertSame($expectedEntry->getZipCode(), $actualEntry->getZipCode());
        $this->assertSame($expectedEntry->getCity(), $actualEntry->getCity());
        $this->assertSame($expectedEntry->getCountry(), $actualEntry->getCountry());
        $this->assertSame($expectedEntry->getPhoneNumber(), $actualEntry->getPhoneNumber());
        $this->assertEquals($expectedEntry->getBirthDay(), $actualEntry->getBirthday());
    }

}
