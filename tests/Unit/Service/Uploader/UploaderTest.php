<?php
declare(strict_types=1);

namespace App\Tests\Unit\Service\Uploader;

use App\Exception\UploadFileException;
use App\Service\FileUploader\FileUploader;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;

final class UploaderTest extends TestCase
{
    /**
     * @var FileUploader
     */
    private $fileUploader;

    /**
     * @var UploadedFile;
     */
    private $uploadedFile;

    protected function setUp()
    {
        $this->fileUploader = new FileUploader('test/path/');
        $this->uploadedFile = $this->createMock(UploadedFile::class);
    }

    public function testSuccessfulUpload(): void
    {
        $this->uploadedFile->expects(self::once())->method('getClientOriginalName')->willReturn('fileTestName');
        $this->uploadedFile->expects(self::once())->method('guessExtension')->willReturn('png');
        $this->uploadedFile->expects(self::once())->method('move');

        $uploadedFileName = $this->fileUploader->upload($this->uploadedFile);

        $this->assertStringContainsString('png', $uploadedFileName);
        $this->assertStringContainsString('fileTestName', $uploadedFileName);
    }

    public function testFailedUpload(): void
    {
        $this->uploadedFile->expects(self::once())->method('getClientOriginalName')->willReturn('fileTestName');
        $this->uploadedFile->expects(self::once())->method('guessExtension')->willReturn('png');
        $this->uploadedFile->method('move')->willThrowException(new FileException());

        $this->expectException(UploadFileException::class);
        $this->fileUploader->upload($this->uploadedFile);

    }
}
