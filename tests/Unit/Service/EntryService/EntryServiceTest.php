<?php
declare(strict_types=1);

namespace App\Tests\Unit\Service\EntryService;

use App\Entity\Entry;
use App\Form\EntryType;
use App\Repository\Entry\EntryRepositoryInterface;
use App\Service\Entry\EntryService;
use App\Service\FileUploader\FileUploader;
use App\Tests\Unit\DataFixture\AddressBookEntriesFixtures;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;

final class EntryServiceTest extends TestCase
{

    /**
     * @var MockObject
     */
    private $entryRepository;

    /**
     * @var MockObject
     */
    private $entityManager;

    /**
     * @var MockObject
     */
    private $formFactory;

    /**
     * @var MockObject
     */
    private $fileUploader;

    /**
     * @var EntryService
     */
    private $entryService;

    protected function setUp()
    {
        $this->entryRepository = $this->createMock(EntryRepositoryInterface::class);
        $this->entityManager = $this->createMock(EntityManagerInterface::class);
        $this->formFactory = $this->createMock(FormFactoryInterface::class);
        $this->fileUploader = $this->createMock(FileUploader::class);
        $this->entryService = new EntryService(
            $this->entryRepository,
            $this->entityManager,
            $this->formFactory,
            $this->fileUploader
        );
    }

    public function testGetAllEntries()
    {
        $this->entryRepository->method('findAll')->willReturn(
            AddressBookEntriesFixtures::getAddressBookEntriesForTesting()
        );
        $this->assertEquals(
            AddressBookEntriesFixtures::getAddressBookEntriesForTesting(),
            $this->entryService->getAllEntries()
        );
    }

    public function testSaveEntry()
    {
        $entityToSave = AddressBookEntriesFixtures::getAddressBookEntriesForTesting()[0];
        $this->entryRepository->expects(self::once())->method('save')->with(
            $entityToSave
        );
        $this->entityManager->expects(self::once())->method('flush');
        $this->entryService->saveEntry($entityToSave);
    }

    public function testDeleteEntry()
    {
        $entityToDelete = AddressBookEntriesFixtures::getAddressBookEntriesForTesting()[0];
        $this->entryRepository->expects(self::once())->method('delete')->with(
            $entityToDelete
        );
        $this->entityManager->expects(self::once())->method('flush');
        $this->entryService->deleteEntry($entityToDelete);
    }

    public function testCreateForm()
    {
        $entry = new Entry();
        $formInterface = $this->createMock(FormInterface::class);
        $this->formFactory->
        expects(self::once())->
        method('create')->
        with(EntryType::class, $entry)->willReturn(
            $formInterface
        );

        $this->entryService->getEntryForm($entry);
    }

    public function testUploadFile()
    {
        $uploadedFile = $this->createMock(UploadedFile::class);
        $form = $this->createMock(FormInterface::class);

        $this->fileUploader->expects(self::once())->method('upload')->with($uploadedFile)->willReturn('fileName');

        $form->method('get')->willReturn($form);
        $form->method('getData')->willReturn($uploadedFile);

        $this->entryService->uploadFile($form, AddressBookEntriesFixtures::getAddressBookEntriesForTesting()[0]);
    }
}
