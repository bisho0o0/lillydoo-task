<?php
declare(strict_types=1);

namespace App\Tests\Unit\Form;

use App\Entity\Entry;
use App\Form\EntryType;
use Symfony\Component\Form\Extension\Validator\ValidatorExtension;
use Symfony\Component\Form\Test\TypeTestCase;
use Symfony\Component\Validator\Validation;

final class EntryTypeTest extends TypeTestCase
{
    protected function getExtensions()
    {
        $validator = Validation::createValidatorBuilder()
            ->enableAnnotationMapping()
            ->getValidator();

        return [
            new ValidatorExtension($validator),
        ];
    }

    public function testSubmitValidData()
    {
        $formData = [
            'firstName' => 'testName',
            'lastName' => 'lastName',
            'email' => 'test@test.com',
            'streetName' => 'street name1',
            'phoneNumber' => '123456789',
            'buildingNumber' => 12,
            'zipCode' => '12345',
            'city' => 'Berlin',
            'country' => 'Germany',
            'birthday' => [
                'day' => '11',
                'month' => '10',
                'year' => '1992',
            ],
        ];

        $entryToCompare = new Entry();

        $form = $this->factory->create(EntryType::class, $entryToCompare);

        $entry = new Entry();

        $entry->setFirstName('testName');
        $entry->setLastName('lastName');
        $entry->setEmail('test@test.com');
        $entry->setStreetName('street name1');
        $entry->setPhoneNumber('123456789');
        $entry->setBuildingNumber(12);
        $entry->setZipCode('12345');
        $entry->setCity('Berlin');
        $entry->setCountry('Germany');
        $entry->setBirthday(new \DateTime('1992-10-11'));

        $form->submit($formData);

        $this->assertTrue($form->isSynchronized());

        $this->assertEquals($entry, $entryToCompare);

        $view = $form->createView();
        $children = $view->children;

        foreach (array_keys($formData) as $key) {
            $this->assertArrayHasKey($key, $children);
        }
    }
}
