<?php
declare(strict_types=1);

namespace App\Tests\Unit\DataFixture;


use App\Entity\Entry;

final class AddressBookEntriesFixtures
{
    static function getAddressBookEntriesForTesting(): array
    {
        $entries = [];

        $entry = new Entry();
        $entry->setId(1);
        $entry->setFirstName('testName');
        $entry->setLastName('lastName');
        $entry->setEmail('test@test.com');
        $entry->setStreetName('street name1');
        $entry->setPhoneNumber('123456789987');
        $entry->setBuildingNumber(1);
        $entry->setZipCode('12345');
        $entry->setCity('Berlin');
        $entry->setCountry('Germany');
        $entry->setBirthday(new \DateTime('1992-10-11'));

        array_push($entries, $entry);

        $entry = new Entry();
        $entry->setId(2);
        $entry->setFirstName('testName2');
        $entry->setLastName('lastName2');
        $entry->setEmail('test2@test.com');
        $entry->setStreetName('street name2');
        $entry->setPhoneNumber('123456789123');
        $entry->setBuildingNumber(2);
        $entry->setZipCode('67891');
        $entry->setCity('Berlin');
        $entry->setCountry('Germany');
        $entry->setBirthday(new \DateTime('2000-10-11'));

        array_push($entries, $entry);

        $entry = new Entry();
        $entry->setId(3);
        $entry->setFirstName('testName3');
        $entry->setLastName('lastName3');
        $entry->setEmail('test3@test.com');
        $entry->setStreetName('street name3');
        $entry->setPhoneNumber('123456789125');
        $entry->setBuildingNumber(3);
        $entry->setZipCode('67893');
        $entry->setCity('Berlin');
        $entry->setCountry('Germany');
        $entry->setBirthday(new \DateTime('1999-10-11'));

        array_push($entries, $entry);

        return $entries;
    }
}
