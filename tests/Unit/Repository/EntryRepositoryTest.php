<?php
declare(strict_types=1);

namespace App\Tests\Unit\Repository;

use App\Entity\Entry;
use App\Repository\Entry\EntryRepository;
use App\Repository\Entry\EntryRepositoryInterface;
use App\Tests\Unit\DataFixture\AddressBookEntriesFixtures;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ObjectRepository;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

final class EntryRepositoryTest extends TestCase
{
    /**
     * @var MockObject
     */
    private $entityManager;

    /**
     * @var EntryRepositoryInterface
     */
    private $entryRepository;

    /**
     * @var MockObject
     */
    private $objectRepository;

    /**
     * @var Entry
     */
    private $addressBookEntry;

    protected function setUp()
    {
        $this->entityManager = $this->createMock(EntityManagerInterface::class);
        $this->objectRepository = $this->createMock(ObjectRepository::class);

        $this->entityManager->method('getRepository')->willReturn($this->objectRepository);

        $this->entryRepository = new EntryRepository($this->entityManager);
        $this->addressBookEntry = AddressBookEntriesFixtures::getAddressBookEntriesForTesting()[0];
    }

    public function testFindAllWithResults(): void
    {
        $result = [$this->addressBookEntry];

        $this->objectRepository->method('findAll')->willReturn($result);

        $this->assertEquals([$this->addressBookEntry], $this->entryRepository->findAll());
    }

    public function testFindAllWithNoResults(): void
    {
        $this->objectRepository->method('findAll')->willReturn([]);

        $this->assertEquals([], $this->entryRepository->findAll());
    }


    public function testFindOneByIdWithResults(): void
    {
        $this->objectRepository->method('find')->with(1)->willReturn($this->addressBookEntry);

        $this->assertEquals($this->addressBookEntry, $this->entryRepository->find(1));
    }

    public function testFindOneByIdWithNoResults(): void
    {
        $this->objectRepository->method('findAll')->willReturn(null);

        $this->assertEquals(null, $this->entryRepository->find(1));
    }

    public function testSaveEntry(): void
    {
        $this->entityManager->expects(self::once())->method('persist')->with($this->addressBookEntry);

        $this->entryRepository->save($this->addressBookEntry);
    }

    public function testDeleteEntity(): void
    {
        $this->entityManager->expects(self::once())->method('remove')->with($this->addressBookEntry);

        $this->entryRepository->delete($this->addressBookEntry);
    }
}
