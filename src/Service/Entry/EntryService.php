<?php
declare(strict_types=1);

namespace App\Service\Entry;

use App\Entity\Entry;
use App\Exception\UploadFileException;
use App\Form\EntryType;
use App\Repository\Entry\EntryRepositoryInterface;
use App\Service\FileUploader\FileUploader;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Form\FormInterface;

final class EntryService
{
    /**
     * @var EntryRepositoryInterface
     */
    private $entryRepository;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var FormFactoryInterface
     */
    private $formFactory;

    /**
     * @var FileUploader
     */
    private $fileUploader;

    /**
     * @param EntryRepositoryInterface $entryRepository
     * @param EntityManagerInterface $entityManager
     * @param FormFactoryInterface $formFactory
     * @param FileUploader $fileUploader
     */
    public function __construct(
        EntryRepositoryInterface $entryRepository,
        EntityManagerInterface $entityManager,
        FormFactoryInterface $formFactory,
        FileUploader $fileUploader
    ) {
        $this->entryRepository = $entryRepository;
        $this->entityManager = $entityManager;
        $this->formFactory = $formFactory;
        $this->fileUploader = $fileUploader;
    }

    /**
     * @return array
     */
    public function getAllEntries(): array
    {
        return $this->entryRepository->findAll();
    }

    /**
     * @param Entry $entry
     */
    public function saveEntry(Entry $entry): void
    {
        $this->entryRepository->save($entry);
        $this->entityManager->flush();
    }

    /**
     * @param Entry $entry
     */
    public function deleteEntry(Entry $entry): void
    {
        $this->entryRepository->delete($entry);
        $this->entityManager->flush();
    }

    /**
     * @param Entry $entry
     * @return FormInterface
     */
    public function getEntryForm(Entry $entry): FormInterface
    {
        return $this->formFactory->create(EntryType::class, $entry);
    }

    /**
     * @param FormInterface $form
     * @param Entry $entry
     * @throws UploadFileException
     */
    public function uploadFile(FormInterface $form, Entry $entry): void
    {
        $photoFile = $form->get(EntryType::PHOTO_NAME_FIELD)->getData();

        if ($photoFile) {
            $photoFileName = $this->fileUploader->upload($photoFile);
            $entry->setPhotoName($photoFileName);
        }
    }
}
