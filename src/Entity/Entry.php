<?php
declare(strict_types=1);

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @ORM\Table(name="entry")
 */
final class Entry
{
    /**
     * @var int
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(type="string", length=100)
     * @Assert\NotBlank(message="Please enter first name")
     */
    private $firstName;

    /**
     * @var string
     * @ORM\Column(type="string", length=100)
     * @Assert\NotBlank(message="Please Enter last name")
     */
    private $lastName;

    /**
     * @var string
     * @ORM\Column(type="string", length=100)
     * @Assert\NotBlank(message="Please enter street name")
     */
    private $streetName;

    /**
     * @var int
     * @ORM\Column(type="integer", length=20)
     * @Assert\NotBlank(message="Please enter building number")
     */
    private $buildingNumber;

    /**
     * @var string
     * @ORM\Column(type="string", length=5)
     * @Assert\NotBlank(message="Please enter zip code")
     * @Assert\Length(exactMessage="Please enter valid zip code with 5 numbers",min="5",max="5")
     */
    private $zipCode;

    /**
     * @var string
     * @ORM\Column(type="string", length=100)
     * @Assert\NotBlank(message="Please enter city")
     */
    private $city;

    /**
     * @var string
     * @ORM\Column(type="string", length=50)
     * @Assert\NotBlank(message="Please enter country")
     */
    private $country;

    /**
     * @var string
     * @ORM\Column(type="string", length=20)
     * @Assert\NotBlank(message="Please enter a valid phone number")
     * @Assert\Length(exactMessage="Please enter 12 numbers",min="12",max="12")
     */
    private $phoneNumber;

    /**
     * @var \DateTime
     * @ORM\Column(type="date")
     * @Assert\NotBlank
     */
    private $birthday;

    /**
     * @var string
     * @ORM\Column(type="string", length=50)
     * @Assert\NotBlank
     * @Assert\Email(message="Please enter valid email")
     */
    private $email;

    /**
     * @var string
     * @ORM\Column(type="string", length=100,nullable=true)
     */
    private $photoName;

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    /**
     * @param string $firstName
     */
    public function setFirstName(string $firstName): void
    {
        $this->firstName = $firstName;
    }

    /**
     * @return string
     */
    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    /**
     * @param string $lastName
     */
    public function setLastName(string $lastName): void
    {
        $this->lastName = $lastName;
    }

    /**
     * @return string
     */
    public function getStreetName(): ?string
    {
        return $this->streetName;
    }

    /**
     * @param string $streetName
     */
    public function setStreetName(string $streetName): void
    {
        $this->streetName = $streetName;
    }

    /**
     * @return int
     */
    public function getBuildingNumber(): ?int
    {
        return $this->buildingNumber;
    }

    /**
     * @param int $buildingNumber
     */
    public function setBuildingNumber(int $buildingNumber): void
    {
        $this->buildingNumber = $buildingNumber;
    }

    /**
     * @return string
     */
    public function getZipCode(): ?string
    {
        return $this->zipCode;
    }

    /**
     * @param string $zipCode
     */
    public function setZipCode(string $zipCode): void
    {
        $this->zipCode = $zipCode;
    }

    /**
     * @return string
     */
    public function getCity(): ?string
    {
        return $this->city;
    }

    /**
     * @param string $city
     */
    public function setCity(string $city): void
    {
        $this->city = $city;
    }

    /**
     * @return string
     */
    public function getCountry(): ?string
    {
        return $this->country;
    }

    /**
     * @param string $country
     */
    public function setCountry(string $country): void
    {
        $this->country = $country;
    }

    /**
     * @return string
     */
    public function getPhoneNumber(): ?string
    {
        return $this->phoneNumber;
    }

    /**
     * @param string $phoneNumber
     */
    public function setPhoneNumber(string $phoneNumber): void
    {
        $this->phoneNumber = $phoneNumber;
    }

    /**
     * @return \DateTime
     */
    public function getBirthday(): ?\DateTime
    {
        return $this->birthday;
    }

    /**
     * @param \DateTime $birthday
     */
    public function setBirthday(\DateTime $birthday): void
    {
        $this->birthday = $birthday;
    }

    /**
     * @return string
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail(string $email): void
    {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getPhotoName(): ?string
    {
        return $this->photoName;
    }

    /**
     * @param string $photoName
     */
    public function setPhotoName(string $photoName): void
    {
        $this->photoName = $photoName;
    }
}
