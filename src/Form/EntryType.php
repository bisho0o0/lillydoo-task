<?php
declare(strict_types=1);

namespace App\Form;

use App\Entity\Entry;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\File;

class EntryType extends AbstractType
{
    const PHOTO_NAME_FIELD = 'photo';

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('firstName', TextType::class)
            ->add('lastName', TextType::class)
            ->add('email', EmailType::class)
            ->add('streetName', TextType::class)
            ->add('buildingNumber', TextType::class)
            ->add('zipCode', TextType::class)
            ->add('city', TextType::class)
            ->add('country', TextType::class)
            ->add('phoneNumber', TextType::class)
            ->add('birthday', DateType::class, ['years' => $this->getYearsList()])
            ->add('email', TextType::class)
            ->add(
                self::PHOTO_NAME_FIELD,
                FileType::class,
                [
                    'label' => 'Photo',
                    'mapped' => false,
                    'required' => false,
                    'constraints' => [
                        new File(
                            [
                                'maxSize' => '1024k',
                                'mimeTypes' => [
                                    'image/jpeg',
                                    'image/png',
                                ],
                                'mimeTypesMessage' => 'Please upload a valid image',
                            ]
                        ),
                    ],
                ]
            );
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults(
            [
                'data_class' => Entry::class,
            ]
        );
    }

    private function getYearsList()
    {
        $years = [];

        $currentYear = date('Y');

        $startingYear = $currentYear - 100;

        for ($i = $startingYear; $i < $currentYear; $i++) {
            array_push($years, $i);
        }

        return $years;
    }
}
