<?php
declare(strict_types=1);

namespace App\DataFixtures;

use App\Entity\Entry;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

final class EntryFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $entry = new Entry();
        $entry->setFirstName('testName');
        $entry->setLastName('lastName');
        $entry->setEmail('test@test.com');
        $entry->setStreetName('street name1');
        $entry->setPhoneNumber('123456789987');
        $entry->setBuildingNumber(1);
        $entry->setZipCode('12345');
        $entry->setCity('Berlin');
        $entry->setCountry('Germany');
        $entry->setBirthday(new \DateTime('1992-10-11'));

        $manager->persist($entry);

        $entry = new Entry();
        $entry->setFirstName('testName2');
        $entry->setLastName('lastName2');
        $entry->setEmail('test2@test.com');
        $entry->setStreetName('street name2');
        $entry->setPhoneNumber('123456789123');
        $entry->setBuildingNumber(2);
        $entry->setZipCode('67891');
        $entry->setCity('Berlin');
        $entry->setCountry('Germany');
        $entry->setBirthday(new \DateTime('2000-10-11'));
        $manager->flush();

        $manager->persist($entry);

        $manager->flush();
    }
}
