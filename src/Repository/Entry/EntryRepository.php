<?php
declare(strict_types=1);

namespace App\Repository\Entry;

use App\Entity\Entry;
use App\Repository\DoctrineRepository;

final class EntryRepository extends DoctrineRepository implements EntryRepositoryInterface
{
    /**
     * @return array
     */
    public function findAll(): array
    {
        return $this->getRepository(Entry::class)->findAll();
    }

    /**
     * @param int $id
     * @return Entry|null
     */
    public function find(int $id): ?Entry
    {
        return $this->getRepository(Entry::class)->find($id);
    }

    /**
     * @param Entry $entry
     */
    public function save(Entry $entry): void
    {
        $this->getEntityManager()->persist($entry);
    }

    /**
     * @param Entry $entry
     */
    public function delete(Entry $entry): void
    {
        $this->getEntityManager()->remove($entry);
    }
}

