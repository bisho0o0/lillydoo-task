<?php
declare(strict_types=1);

namespace App\Repository\Entry;

use App\Entity\Entry;

interface EntryRepositoryInterface
{
    /**
     * @return array
     */
    public function findAll(): array;

    /**
     * @param int $id
     * @return Entry|null
     */
    public function find(int $id): ?Entry;

    /**
     * @param Entry $entry
     */
    public function save(Entry $entry): void;

    /**
     * @param Entry $entry
     */
    public function delete(Entry $entry): void;

}
