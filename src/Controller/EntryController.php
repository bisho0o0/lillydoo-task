<?php
declare(strict_types=1);

namespace App\Controller;

use App\Entity\Entry;
use App\Exception\UploadFileException;
use App\Service\Entry\EntryService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/entry")
 */
class EntryController extends Controller
{
    /**
     * @var EntryService
     */
    private $entryService;

    /**
     * @param EntryService $entryService
     */
    public function __construct(EntryService $entryService)
    {
        $this->entryService = $entryService;
    }

    /**
     * @Route("/", name="entry_index", methods={"GET"})
     */
    public function index(): Response
    {
        $entries = $this->entryService->getAllEntries();

        return $this->render(
            'entry/index.html.twig',
            [
                'entries' => $entries,
            ]
        );
    }

    /**
     * @Route("/new", name="entry_new", methods={"GET","POST"})
     * @param Request $request
     * @return Response
     * @throws UploadFileException
     */
    public function new(Request $request): Response
    {
        $entry = new Entry();

        $form = $this->entryService->getEntryForm($entry);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $this->entryService->uploadFile($form, $entry);

            $this->entryService->saveEntry($entry);

            return $this->redirectToRoute('entry_index');
        }

        return $this->render(
            'entry/new.html.twig',
            [
                'entry' => $entry,
                'form' => $form->createView(),
            ]
        );
    }

    /**
     * @Route("/{id}", name="entry_show", methods={"GET"})
     * @param Entry $entry
     * @return Response
     */
    public function show(Entry $entry): Response
    {
        return $this->render(
            'entry/show.html.twig',
            [
                'entry' => $entry,
            ]
        );
    }

    /**
     * @Route("/{id}/edit", name="entry_edit", methods={"GET","POST"})
     * @param Request $request
     * @param Entry $entry
     * @return Response
     * @throws UploadFileException
     */
    public function edit(Request $request, Entry $entry): Response
    {
        $form = $this->entryService->getEntryForm($entry);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $this->entryService->uploadFile($form, $entry);

            $this->entryService->saveEntry($entry);

            return $this->redirectToRoute('entry_index');
        }

        return $this->render(
            'entry/edit.html.twig',
            [
                'entry' => $entry,
                'form' => $form->createView(),
            ]
        );
    }

    /**
     * @Route("/{id}", name="entry_delete", methods={"DELETE"})
     * @param Request $request
     * @param Entry $entry
     * @return Response
     */
    public function delete(Request $request, Entry $entry): Response
    {
        if ($this->isCsrfTokenValid('delete'.$entry->getId(), $request->request->get('_token'))) {
            $this->entryService->deleteEntry($entry);
        }

        return $this->redirectToRoute('entry_index');
    }
}
